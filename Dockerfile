FROM openjdk:8-jdk-slim
COPY "./target/MX_UNO_WSD_Olimpicos-0.0.1-SNAPSHOT.jar" "app.jar"
EXPOSE 8070
ENTRYPOINT ["java","-jar","app.jar"]