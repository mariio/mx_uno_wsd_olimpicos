package mx.com.amx.unotv.olimpicos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;

import mx.com.amx.unotv.olimpicos.dao.YoutubeDAO;
import mx.com.amx.unotv.olimpicos.dto.TransmitionDTO;
import mx.com.amx.unotv.olimpicos.exception.DAOException;

@RestController
@RequestMapping
public class YoutubeController {

	
	@Autowired
	YoutubeDAO youtubeDAO;
	
	@PostMapping(value="insertTransmition")
	public boolean insertTransmition(@RequestBody TransmitionDTO transmition) throws RestClientException {
		
		boolean response = false;
		try {
			
			response = youtubeDAO.insertTransmition(transmition);
			
		}catch (DAOException e) {
			
			throw new RestClientException(e.getMessage());

		}catch (Exception e) {

		
			throw new RestClientException(e.getMessage());
		}
		
		return response;
	}
	
	
}
