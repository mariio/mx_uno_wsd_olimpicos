package mx.com.amx.unotv.olimpicos.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;



public class TransmitionDTO {
	
	
	private String fechaFin;
	
	private String fechaInicio;
	
	private String duracion;
	
	private String titulo;
	
	private String descripcion;
	
	private String RCS;
		
	private String live;
	
	private String ingestion;
	
	private String frameRate;
	
	private String resolucion;
	
	
	private String idTransmition;

	
	
	
	
	
	public String getIdTransmition() {
		return idTransmition;
	}
	public void setIdTransmition(String idTransmition) {
		this.idTransmition = idTransmition;
	}
	public String getResolucion() {
		return resolucion;
	}
	public void setResolucion(String resolucion) {
		this.resolucion = resolucion;
	}
	public String getFrameRate() {
		return frameRate;
	}
	public void setFrameRate(String frameRate) {
		this.frameRate = frameRate;
	}
	public String getIngestion() {
		return ingestion;
	}
	public void setIngestion(String ingestion) {
		this.ingestion = ingestion;
	}
	public String getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	public String getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public String getDuracion() {
		return duracion;
	}
	public void setDuracion(String duracion) {
		this.duracion = duracion;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getRCS() {
		return RCS;
	}
	public void setRCS(String rCS) {
		RCS = rCS;
	}
	public String getLive() {
		return live;
	}
	public void setLive(String live) {
		this.live = live;
	}
	
}
