package mx.com.amx.unotv.olimpicos.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.stereotype.Component;

import mx.com.amx.unotv.olimpicos.dto.TransmitionDTO;
import mx.com.amx.unotv.olimpicos.exception.DAOException;

@Component
public class YoutubeDAO {

	
	
	@Autowired
	private JdbcTemplate jdbcTemplate;	
	
	public boolean insertTransmition(TransmitionDTO transmition) throws DAOException {
	
		boolean success = false;
		int insert = 0;
		StringBuffer sbQuery = new StringBuffer();

		
		
		try {
			
			sbQuery.append(" insert into uno_mx_olimpicos_youtube");   
			sbQuery.append(" (FD_FECHA_INICIO,FD_FECHA_FIN,FC_DURACION,FC_TITULO,FC_DESCRIPCION,FC_RCS,FC_ID_YOUTUBE,FI_LIVE_YT) ");   
			sbQuery.append(" values ( ?,?,?,?,?,?,?,? )");   
	 
			insert =  jdbcTemplate.update(new PreparedStatementCreator() {
				
				@Override
				public PreparedStatement createPreparedStatement(Connection con) throws SQLException {

					PreparedStatement ps = con.prepareStatement(sbQuery.toString());
					
					ps.setString( 1, transmition.getFechaInicio());
					ps.setString( 2, transmition.getFechaFin());
					ps.setString( 3, transmition.getDuracion());
					ps.setString( 4, transmition.getTitulo());
					ps.setString( 5, transmition.getDescripcion());
					ps.setString( 6, transmition.getRCS());
					ps.setString( 7, transmition.getIdTransmition());
					ps.setString( 8, transmition.getLive());
					return ps;
				}
			});
			
			if(insert >0) {
				
				success = true;
				
			}	
		}catch (Exception e) {


			throw new DAOException(e.getMessage());
			
		}
		
		return success;
	}
	
	
}
