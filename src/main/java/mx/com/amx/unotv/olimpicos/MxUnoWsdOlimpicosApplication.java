package mx.com.amx.unotv.olimpicos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MxUnoWsdOlimpicosApplication {

	public static void main(String[] args) {
		SpringApplication.run(MxUnoWsdOlimpicosApplication.class, args);
	}

}
